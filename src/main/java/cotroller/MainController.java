package cotroller;

import data.ClientDataService;
import entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.List;

@EnableWebMvc
@RestController
public class MainController {

    @Autowired
    ClientDataService service;

    @RequestMapping(method = RequestMethod.GET, value = "rest/user")
    public List<Client> getAll(){
        return service.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/rest/user/{id}")
    public Client getClient(@PathVariable Long id){
        return service.findById(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "rest/user")
    public void delete(Client client){
        service.delete(client);
    }

    @RequestMapping(method = RequestMethod.POST, value = "rest/user")
    public Client addClient(Client client){
        return service.addClient(client);
    }

    @RequestMapping(method = RequestMethod.PUT, value = "rest/user")
    public Client editClient(Client client){
        return service.editClient(client);
    }
    
}
