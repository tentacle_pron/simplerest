package entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Clients")
public class Client {

    @Id
    @GeneratedValue
    @NotNull
    private long id;

    @NotNull
    private String surname;

    @NotNull
    private String name;

    @NotNull
    private String patronymic;

    @NotNull
    private Date dateOfBirth;

    @NotNull
    private String passportSeries;

    @NotNull
    private String issuedBy;

    @NotNull
    private Date issuingDate;

    @NotNull
    private String placeOfBirth;

    @NotNull
    @ElementCollection
    private List<String> residenceCities;

    @NotNull
    private String residentialAddress;

    private String phone;

    private String email;

    private String placeOfWork;

    private String position;

    @NotNull
    private String placeOfResidence;

    @NotNull
    @ElementCollection
    private List<String> maritalStatus;

    @NotNull
    @ElementCollection
    private List<String> citizenship;

    @NotNull
    @ElementCollection
    private List<String> disability;

    private boolean retiree;

    @NotNull
    private BigDecimal monthlyIncome;


}
