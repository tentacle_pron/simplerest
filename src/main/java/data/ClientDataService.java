package data;

import entity.Client;

import java.util.List;

public interface ClientDataService {

    Client addClient(Client client);
    void delete(Client client);
    Client findById(long id);
    Client editClient(Client client);
    List<Client> getAll();
}
