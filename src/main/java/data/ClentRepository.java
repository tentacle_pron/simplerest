package data;

import entity.Client;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClentRepository extends CrudRepository<Client, Long> {

}
