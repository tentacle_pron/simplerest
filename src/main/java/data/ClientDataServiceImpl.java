package data;

import entity.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientDataServiceImpl implements ClientDataService{

    @Autowired
    private ClentRepository repository;


    @Override
    public Client addClient(Client client) {
        return repository.save(client);
    }

    @Override
    public void delete(Client client) {
        repository.delete(client);
    }

    @Override
    public Client findById(long id) {
        return repository.findById(id).orElseThrow();
    }

    @Override
    public Client editClient(Client client) {
        return repository.save(client);
    }

    @Override
    public List<Client> getAll() {
        return (List<Client>) repository.findAll();
    }
}
